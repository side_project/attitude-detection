# attitude-detection

可以比對兩段影片中指定範圍片段之肢體動作相似性

基於 [TensorFlow.js Pose estimation](https://www.tensorflow.org/lite/examples/pose_estimation/overview)，並使用 MoveNet 模型實現骨架偵測

相似度評估使用基於關節權重之餘弦相似度（Cosine similarity）法完成。

基本流程：
1. 選擇正確影片與待測影片
2. 滑鼠在時間軸上移動時，可以即時預覽當前影格圖像
3. 左鍵設定「比較起點」，右鍵設定「比較終點」
4. 兩個影片都設定玩起點與終點後，等待時間軸擷取影格
5. 影格擷取完成後，即可點擊「開始比對」
6. 觀看「相似度結果」

[網頁連結](https://attitude-detection-side-project-f59e4f936210357256c22cce13a7dda.gitlab.io/home)